import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextWebpackPlugin from 'extract-text-webpack-plugin';

const PATHS = {
  src: path.join(__dirname, '../src'),
  dist: path.join(__dirname, '../dist')
};

export default {
  context: __dirname,
  mode: 'development',
  entry: {
    app: [PATHS.src]
  },
  output: {
    path: PATHS.dist,
    filename: '[name].js',
    publicPath: '/'
  },
  optimization: {},
  resolve: {},
  devtool: 'eval-sourcemap',
  module: {
    rules: [
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'resolve-url-loader', 'less-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${PATHS.src}/index.html`
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: PATHS.dist,
    compress: true,
    headers: {
        'X-Content-Type-Options': 'nosniff',
        'X-Frame-Options': 'DENY'
    },
    open: true,
    overlay: {
        warnings: true,
        errors: true
    },
    port: 8081,
    publicPath: 'http://localhost:8081/',
    hot: true
}
};