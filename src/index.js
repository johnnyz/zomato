'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './js/app';
import store from './js/store';

import './style/app.less';

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('main')
);