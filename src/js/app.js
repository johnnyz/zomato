'use strict';

import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import { ConnectedRouter as Router } from 'react-router-redux';

import { history } from './store';
import Page404 from './routers/404'
import MainPanel from './containers/mainPanel';

const routes = [
  { path: '/', component: MainPanel },
  { path: '/404', component: Page404, exact: true }
];

class App extends React.Component {
  render () {
    return (
      <React.Fragment>
        <Router history={history}>
          <Fragment>
            {routes.map((route, key) => (
              <Route key={key} {...route} />
            ))}
          </Fragment>  
        </Router>
      </React.Fragment>
    );
  }
}

export default App;