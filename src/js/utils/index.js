import { assignInWith, partialRight } from 'lodash';

export const extendIfPropertyHasValue = partialRight(assignInWith, (objValue, srcValue) => srcValue || objValue);