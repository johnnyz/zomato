'use strict';

import React from 'react';
import { NavLink } from 'react-router-dom';

class SideList extends React.Component {
  render () {
    return (
      <React.Fragment>
        <section className="sidepanel-section"><span className="sidepanel-item label">{this.props.label}</span></section>
        {this.props.list.map((item, key) => (
          <section key={key} className="sidepanel-section clearfix">
            <NavLink to={`/restaurant/${item.id}`} activeClassName="active" exact={true} className="sidepanel-item" title={item.name}>{item.name}</NavLink>
          </section>
        ))}
      </React.Fragment>
    );
  }
}

export default SideList;