'use strict';

import React, { Component } from 'react';

class Detail extends Component {
  render () {
    const { 
      name, cuisines, location, featured_image, phone_numbers, 
      has_online_delivery, has_table_booking } = this.props.details;

    const imageStyle = {
      backgroundImage: `url(${featured_image})`,
      backgroundPosition: 'center center',
      backgroundSize: 'cover'
    };
    
    return (
      <section className="details-panel-container">
        <div className="details-panel">
          {!!featured_image?<div className="image" style={imageStyle}></div>:''}
          <div className="details">
            <h3 className="details-name">{name}</h3>
            <h4 className="details-address">{location.address}</h4>
            <div className="details-features clearfix">
              {
              !!has_table_booking?
              <span className="features"><i className="fas fa-check"></i>Bookings available</span>:
              <span className="features"><i className="fas fa-times"></i>No bookings</span>
              }
              {
              !!has_table_booking?
              <span className="features"><i className="fas fa-check"></i>Delivery available</span>:
              <span className="features"><i className="fas fa-times"></i>No Delivery</span>
              }
            </div>
            <div className="details-info">
              <span className="info-label">CUISINES</span>
              <span className="info-value">{cuisines}</span>
            </div>
            
            <div className="details-info">
              <span className="info-label">PHONE NUMBER</span>
              <span className="info-value">0425 729 534</span>
            </div>

            <div className="details-info">
              <span className="info-label">OPENING HOURS</span>
              <span className="info-value">Today 6:30AM to 4PM <span className="time-info">OPEN NOW</span></span>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Detail;