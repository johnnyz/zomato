import React, { Fragment } from 'react';
import { chunk, map, nth } from 'lodash';
import PropTypes from 'prop-types';
import Check, { CheckboxGroup as Group, Checkbox } from 'react-checkbox-group';

class CheckboxGroup extends React.Component {
  state = {
    values: []
  }

  render() {
    const { search, items, title, row, column, direction } = this.props;
    let indexMatrix = [];
    if (direction === 'column') {
      const columnIndexes = [...new Array(column).keys()];
      const rowIndexes = [...new Array(row).keys()];
      indexMatrix = map(rowIndexes, rowIndex => map(columnIndexes, columnIndex => rowIndex + columnIndex*row));
      
    } else {
      indexMatrix = chunk([...new Array(row*column).keys()], column);
    }

    return (
      <Fragment>
        <Group className="checkbox-group" checkboxDepth={6} value={this.state.value} name="checkbox-group" onChange={values => search(values)}>
          <table className="group-container">
            <thead><tr><th colSpan={row} className="group-title">{title}</th></tr></thead>
            <tbody>
              {indexMatrix.map((row, key) => (
                <tr key={key}>
                  {row.map((index, key) => {
                    const item = nth(items, index);
                    return !item?
                    (<td key={index} className="non-item"></td>):
                    (<td key={index} className="group-item">
                      <label><Checkbox value={item.id}></Checkbox>&nbsp;{item.label}</label>
                    </td>);
                  })}
                </tr>
              ))}
            </tbody>
          </table>
        </Group>
      </Fragment>
    );
  }
}

CheckboxGroup.propTypes = {};

export default CheckboxGroup;
