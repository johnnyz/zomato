import React, { Component } from 'react';
import Slider, { Range } from 'rc-slider';

export default class RangeSlider extends Component {
  render() {
    const {title, min, max, minLabel, maxLabel } = this.props;
    return (
      <div className="slider">
        <p className="title">{title}</p>
        <Range min={min} max={max} defaultValue={[min, min+1]} allowCross={false} />
        <p className="labels clearfix"><span className="min-label">{minLabel}</span><span className="max-label">{maxLabel}</span></p>
      </div>
    )
  }
};
