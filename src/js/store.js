import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { createHashHistory as createHistory} from 'history';

import reducer from './reducers';

const history = createHistory();
const middlewares = [thunkMiddleware, routerMiddleware(history)];
const createStoreFn = applyMiddleware(...middlewares)(createStore);

const store = createStoreFn(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export { history };
export default store;