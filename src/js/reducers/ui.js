'use strict';

import { createReducer } from 'redux-create-reducer';
import { extend, first, map, filter, slice, reduce } from 'lodash';

import { FETCH_LANDINGPAGE, GET_RESTAURANT, SEARCH_RESTAURANT } from '../actions/ui';
import { zomatoState } from './index';

export const uiReducer = createReducer({search: {}}, {
  [FETCH_LANDINGPAGE](state, action) {
    const zomato = action.payload[zomatoState];
    const { search } = action.payload;
    let { restaurants, categories, cuisines, searchResultMeta } = zomato;
    
    categories = filter(map(categories.categories, item => {
      const { name } = item.categories;
      if (name === 'Delivery') {
        return extend({}, item.categories, { label: 'Delivery' });
      } else if (name === 'Takeaway') {
        return extend({}, item.categories, { label: 'Take-away' });
      } else if (name === 'Dinner') {
        return extend({}, item.categories, { label: 'Dinning' });
      } else if (name === 'Pubs & Bars') {
        return extend({}, item.categories, { label: 'Pubs & Bars' });
      } else {
        return null;
      }
    }), item => !!item);

    let restCuisines = [];
    cuisines = cuisines.cuisines;
    restCuisines = slice(cuisines, 10);
    cuisines = slice(cuisines, 0, 10);
    
    cuisines = map(cuisines, item => ({
      id: item.cuisine.cuisine_id,
      label: item.cuisine.cuisine_name
    })).concat([]);

    let rest = reduce(restCuisines, (rest, item) => {
      rest.id.push(item.cuisine.cuisine_id);
      return rest;
    }, { label: 'Other', id: [] });
    rest.id = rest.id.join(',');

    cuisines.push(rest);
    
    return extend({}, state, {
      restaurants, categories, cuisines, search: extend({}, search, {
        start: searchResultMeta.results_start,
        count: searchResultMeta.results_shown
      })
    });
  },
  [GET_RESTAURANT](state, action) {
    const zomato = action.payload[zomatoState];
    const { restaurantDetails } = zomato;
    return extend({}, state, {
      restaurants: [restaurantDetails].concat(state.restaurants)
    })
  }
});