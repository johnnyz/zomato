'use strict';

import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { zomatoReducer as zomato } from './zomato';
import { uiReducer as ui } from './ui';

const reducer = combineReducers({
  router, zomato, ui
});

export const zomatoState = 'zomato';
export const uiState = 'ui';
export const routerState = 'router';
export default reducer;