'use strict';

import { createReducer } from 'redux-create-reducer';
import { extend } from 'lodash';

import {
  UPDATE_ZOMATO
} from '../actions/zomato';

export const zomatoReducer = createReducer({}, {
  [UPDATE_ZOMATO](state, action) {
    return extend({}, state, action.payload);
  }
});