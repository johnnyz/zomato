import { createSelector } from 'reselect';
import { first, find } from 'lodash';
import { matchPath } from 'react-router-dom';

import { routerState, uiState } from '../reducers';

const getUIState = state => state[uiState];
const getRouterState = state => state[routerState];

export const getCurrentRestaurant = createSelector(
  getUIState,
  getRouterState,
  (ui, router) => {
    
    const { restaurants } = ui;
    const { location } = router;
    const match = matchPath(router.location.pathname, {
      path: '/restaurant/:id',
      exact: true,
      strict: false
    });

    if (!match) {
      return first(restaurants);
    } else {
      return find(restaurants, item => {
        return parseInt(item.id) === parseInt(match.params.id); 
      });
    }
  }
);

export const getRestaurants = createSelector(
  getUIState,
  ui => {
    const { restaurants } = ui;
    return restaurants;
  }
);