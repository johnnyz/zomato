import Promise from 'bluebird';
import { extend, first, defaults, find, has, map, get } from 'lodash';
import { push } from 'react-router-redux';
import {
  fetchCategories, searchRestaurants, fetchLocation, fetchRestaurantDetail, fetchCuisines, fetchDailyMenu 
} from './zomato';
import { zomatoState, uiState } from '../reducers';

//UI
export const FETCH_LANDINGPAGE = '@UI/FETCH_LANDINGPAGE';
export const GET_RESTAURANT = '@UI/GET_RESTAURANT';
export const SEARCH_RESTAURANT = '@UI/SEARCH_RESTAURANT';

const MAX_COUNT = 30;
const COUNTRY = 'Australia';
const CITY = 'Adelaide';
const LOCATION_TYPE = 'city';

export const fetchLandingPageFactory = (search, hooks={pre:[],post:[]}) => () => (dispatch, getState) => {
  const preDispatches = Promise.all(map(get(hooks, 'pre') || [], item => dispatch(item)));
  
  const location = has(getState(), [zomatoState, 'city'])?Promise.resolve():dispatch(fetchLocation(CITY, COUNTRY));

  return preDispatches.then(() => location).then(() => {
    const { city } = getState()[zomatoState];
    search = defaults({}, search, { start: 0, count: MAX_COUNT, ['entity_id']: city['entity_id'], ['entity_type']: LOCATION_TYPE });

    const restFetches = 
    has(getState(), [zomatoState, 'categories'])?
    [dispatch(searchRestaurants(search))]:
    [dispatch(searchRestaurants(search)), dispatch(fetchCategories()), dispatch(fetchCuisines(city['entity_id']))];
    return Promise.all(restFetches);
  })
  .then(() => dispatch({ type: FETCH_LANDINGPAGE, payload: { [zomatoState]: getState()[zomatoState], search } }))
  .then(() => {
    const dispatches = _.find(hooks, 'post') || [];
    return Promise.all(map(dispatches, item => dispatch(item)));
  });
}

export const initUI = fetchLandingPageFactory();

export const getRestaurant = id => (dispatch, getState) => {
  return dispatch(fetchRestaurantDetail(id)).then(() => dispatch({
    type: GET_RESTAURANT,
    payload: { [zomatoState]: getState()[zomatoState] }
  })).catch(() => dispatch(push('/404')))
};

