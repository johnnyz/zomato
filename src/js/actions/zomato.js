import Promise from 'bluebird';
import { extend, find, map } from 'lodash';
import { 
  fetchCategories as categories, 
  searchRestaurants as search, 
  getRestaurant,
  getDailyMenu,
  getCuisines,
  getLocation
} from '../api';

//Zomato API
export const UPDATE_ZOMATO = '@ZOMATO/UPDATE'

export const fetchCategories = () => dispatch => {
  return categories().then(categories => dispatch({
    type: UPDATE_ZOMATO,
    payload: { categories }
  }));
};

export const searchRestaurants = params => dispatch => {
  return search(params).then(resp => {
    const { restaurants, ...searchResultMeta } = resp;
    return dispatch({
      type: UPDATE_ZOMATO,
      payload: { restaurants: map(restaurants, item => item.restaurant), searchResultMeta }
    });
  });
};

export const fetchRestaurantDetail = id => dispatch => {
  return getRestaurant(id).then(restaurantDetails => dispatch({
    type: UPDATE_ZOMATO,
    payload: { restaurantDetails }
  }));
};

export const fetchDailyMenu = id => dispatch => {
  return getDailyMenu(id).then(dailyMenu => dispatch({
    type: UPDATE_ZOMATO,
    payload: { dailyMenu }
  }));
};

export const fetchLocation = (cityName, country) => dispatch => {
  return getLocation(cityName).then(resp => dispatch({
    type: UPDATE_ZOMATO,
    payload: { city: find(resp['location_suggestions'], { 'country_name': country }) }
  }));
};

export const fetchCuisines = cityId => dispatch => {
  return getCuisines(cityId).then(cuisines => dispatch({
    type: UPDATE_ZOMATO,
    payload: { cuisines }
  }));
};
