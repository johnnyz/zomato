'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import { connect } from 'react-redux';
import { extend, debounce } from 'lodash';
import { Route } from 'react-router-dom';
import { push } from 'react-router-redux';
import QueryString from 'query-string';

import CheckboxGroup from '../presentational/checkboxGroup';
import RangeSlider from '../presentational/slider';
import { getRestaurant, fetchLandingPageFactory, initUI } from '../actions/ui';
import { getCurrentRestaurant, getRestaurants } from '../selectors/restaurant'
import { extendIfPropertyHasValue } from '../utils';
import { uiState } from '../reducers';

import SideList from '../presentational/sideList';
import Detail from '../presentational/detail';

const routes = [
  { path: '/', exact: true },
  { path: '/restaurant/:id', exact: true }
];

class MainPanel extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      restaurants: [],
      restaurant: { location: { address: '' }},
      categories: [],
      cuisines: []
    }
  }

  search = (type, values) => {
    const { dispatch } = this.props;
    const { start, count, entity_id, entity_type, ...rest } = this.props.search;
    const search = extend({}, rest, {[type]: values.join(',')});
    const searchFn = fetchLandingPageFactory(search, {
      pre: [push(`/?${QueryString.stringify(search)}`)]
    });
    dispatch(searchFn());
  }

  componentDidMount() {
    this.props.dispatch(initUI());
  }

  static getDerivedStateFromProps(props, state) {
    const { restaurant, restaurants, cuisines, categories } = props;
    if (!restaurant) {
      return state;
    } else {
      return state.categories.length === 0 && state.cuisines.length === 0?
      extend({}, state, { restaurant, restaurants, categories, cuisines }):
      extend({}, state, { restaurant, restaurants });
    }
  }
  
  render() {
    const mainRender = () => (
      <React.Fragment>
        <aside className="side-panel-container">
          <div className="side-panel">
            <SideList label="RESULTS" list={this.state.restaurants} />
          </div>
        </aside>
        <Detail details={this.state.restaurant} />
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <header className="header-container">
          <div className="header clearfix">
            <CheckboxGroup search={values => this.search('category', values)} title="CATEGORY" items={this.state.categories} row={4} column={1} direction="column"/>
            <CheckboxGroup search={values => this.search('cuisines', values)} title="CUISINE" items={this.state.cuisines}  row={4} column={3} direction="column"/>
            <div className="slider-group">
              <RangeSlider title="RATING" min={0} max={5} minLabel="0" maxLabel="5"></RangeSlider>
              <RangeSlider title="COST" min={0} max={4} minLabel="$" maxLabel="$$$$"></RangeSlider>
            </div>
          </div>
        </header>
        <Route render={mainRender} path="/" exact={true}/>
        <Route render={mainRender} path="/restaurant/:id" exact={true}/>
      </React.Fragment>
    );
  }
}

MainPanel = connect((state, ownProps) => ({
  restaurant: getCurrentRestaurant(state),
  restaurants: getRestaurants(state),
  categories: state[uiState].categories,
  cuisines: state[uiState].cuisines,
  search: state[uiState].search
}))(MainPanel);

export default MainPanel;