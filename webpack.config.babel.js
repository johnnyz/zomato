import devConfig from './config/webpack.config.dev';
import prodConfig from './config/webpack.config.prod';

const ENV = process.env.NODE_ENV || 'dev';

let config = {};

if (ENV === 'dev') {
  config = devConfig;
}

else if (ENV === 'prod') {
  config = prodConfig;
}

export default config;


