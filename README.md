#ZOMATO

##Prerequirements

Node >= V10.3.0
NPM >= 6.1.0

##Build

1. npm install
2. npm run build
3. go to http://localhost:8080

##Development

1. npm install
2. npm run dev
3. go to http://localhost:8081

##TODO

1. add test and lint
2. add proptype check
3. refactoring over-engineered redux state management system ¯\\\_(ツ)_/¯
4. add backend server instead of calling API strateaway
5. optimise the UI - add "load more" button in search results bar and support router properly

